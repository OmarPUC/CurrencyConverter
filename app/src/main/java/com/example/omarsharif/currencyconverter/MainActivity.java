package com.example.omarsharif.currencyconverter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    TextView usd;
    TextView rupee;
    TextView riyal;
    TextView dirham;
    TextView euro;
    TextView ringgit;
    TextView AUD;
    TextView QAR;
    TextView KD;
    TextView baht;
    TextView pound;
    TextView baharain;
    TextView orial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.input);
        usd = findViewById(R.id.usd);
        rupee = findViewById(R.id.rupee);
        riyal = findViewById(R.id.riyal);
        dirham = findViewById(R.id.dirham);
        euro = findViewById(R.id.euro);
        ringgit = findViewById(R.id.ringgit);
        AUD = findViewById(R.id.AUD);
        QAR = findViewById(R.id.QAR);
        KD = findViewById(R.id.KD);
        baht = findViewById(R.id.baht);
        pound = findViewById(R.id.pound);
        baharain = findViewById(R.id.baharain);
        orial = findViewById(R.id.orial);

    }

    public void result(View view) {
        editText.getText().toString();

        double takaInDouble = Double.parseDouble(editText.getText().toString());
        double dollarInDouble = takaInDouble * .012;
        double rupeeInDouble = takaInDouble * .82;
        double riyalInDouble = takaInDouble * .045;
        double dirhamInDouble = takaInDouble * .044;
        double euroInDouble = takaInDouble * .010;
        double ringgitInDouble = takaInDouble * .048;
        double audInDouble = takaInDouble * .016;
        double qarInDouble = takaInDouble * .043;
        double kdInDouble = takaInDouble * .0036;
        double bahtInDouble = takaInDouble * .39;
        double poundInDouble = takaInDouble * .0090;
        double baharainInDouble = takaInDouble * .0045;
        double orialInDouble = takaInDouble * .0046;

        String dollar = String.valueOf(dollarInDouble);
        usd.setText("US Dollar: " + dollar + "$");

        String irupee = String.valueOf(rupeeInDouble);
        rupee.setText("\nIndian Rupee: " + irupee + "₹");

        String sriyal = String.valueOf(riyalInDouble);
        riyal.setText("\nSaudi Riyal: " + sriyal + "ر.س");

        String uaedirham = String.valueOf(dirhamInDouble);
        dirham.setText("\nUAE Dirham: " + uaedirham + "د.إ");

        String euroD = String.valueOf(euroInDouble);
        euro.setText("\nEuro: " + euroD + "€");

        String mringgit = String.valueOf(ringgitInDouble);
        ringgit.setText("\nMalaysian Ringgit: " + mringgit + "RM");

        String dAUD = String.valueOf(audInDouble);
        AUD.setText("\nAustralian Dollar: " + dAUD + "$");

        String rQAR = String.valueOf(qarInDouble);
        QAR.setText("\nQatari Rial: " + rQAR + "ر.ق");

        String kdi = String.valueOf(kdInDouble);
        KD.setText("\nKuwaiti Dinar: " + kdi + "د.ك");

        String tBaht = String.valueOf(bahtInDouble);
        baht.setText("\nThai Baht: " + tBaht + "฿");

        String ukPound = String.valueOf(poundInDouble);
        pound.setText("\nPound sterling: " + ukPound + "£");

        String bDinar = String.valueOf(baharainInDouble);
        baharain.setText("\nBahraini Dinar: " + bDinar + ".د.ب");

        String oman = String.valueOf(orialInDouble);
        orial.setText("\nOmani Rial: " + oman + "ر.ع.");


        //        Toast.makeText(MainActivity.this, "Indian Rupee: " + rupeeInDouble, Toast.LENGTH_LONG).show();
    }
}
